query getAllUsers {
  users {
    id
    name
    email
  }
}

query getAllProducts {
  products {
    id
    name
    category{
      id
      name
    }
  }
}

query getProductById {
  product(id: 3) {
    id
    name
    category{
      id
      name
    }
  }
}

mutation newProduct {
  createProduct(name:"Jabon",desc:"para limpiar",price:12.3,category_id:1){
    name
    desc
    price
    category {
      id
      name
    }
  }
}

mutation modifyProduct {
  modifyProduct(id:1,name:"Jabones",desc:"para limpiarss",price:15.3,category_id:2){
    name
    desc
    price
    category {
      id
      name
    }
  }
}
