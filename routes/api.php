<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use App\Http\Controllers\ProductController;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\User;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//1. List all users
Route::get('/user', function () {
    return new UserCollection(User::all());
});

//1. List all products
Route::get('/product', function () {
    return new ProductCollection(Product::all());
});

//2. Get a product by his id
Route::get('/product/{id}', [ProductController::class, 'retriveById']);

//3.Crear un nuevo producto
Route::post('/product', [ProductController::class, 'store']);

//4.Modificiar un product
Route::put('/product/{id}', function (Request $request, $id) {
    /*$prod = Product::find($id);
    $param = $request->input('param');
    $newValue = $request->input('value');

    $prod->$param = $newValue;
    $prod->save();
    return new ProductResource($prod);*/
    
    //shortcut
    //la forma siguiente solo funciona en caso de enviarlo mediante params, por el apartado body no acepta request OLD
    //per fer-ho ho hem de fer en el apartat body, raw i ficar-ho en format json
    Product::whereId($id)->first()->update($request->all());
    return Product::find($id);
});

//5.delete product
Route::delete('/product/{id}', function (Request $request, $id) {
    $prod = Product::find($id)->delete();
    return Product::find($prod);
});

//6.Create user
Route::post('/user', function (Request $request) {
    $user = new User();
    $user->create($request->all());
    return User::find($user);
});

//7.Change user data
Route::put('/user/{id}', function (Request $request, $id) {
    /*$prod = Product::find($id);
    $param = $request->input('param');
    $newValue = $request->input('value');

    $prod->$param = $newValue;
    $prod->save();
    return new ProductResource($prod);*/
    
    //shortcut
    $user = User::whereId($id)->update($request->all());
    return User::find($user);
});


//8.delete product
Route::delete('/user/{id}', function (Request $request, $id) {
    $user = User::find($id)->delete();
    return User::find($user);
});
